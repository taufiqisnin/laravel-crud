@extends('adminlte/master')
@section('content')
<div class="card-header">
    <h3 class="card-title">Edit Form Cast {{$cast->id}}</h3>
  </div>
<form role="form" action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" id="nama" name="nama" value="{{old('nama', $cast->nama)}}" placeholder="Enter nama">
        </div>
        <div class="form-group">
        <label>Umur</label>
        <input type="text" class="form-control" id="umur" name="umur" value="{{old('umur', $cast->umur)}}" placeholder="Enter umur">
        </div>
      <div class="form-group">
        <label>Bio</label>
        <textarea type="text" class="form-control" rows="3" id="bio" name="bio" value="{{old('bio', $cast->bio)}}" placeholder="Enter Bio"></textarea>
      </div>

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Update</button>
    </div>
  </form>
@endsection