<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/', 'HomeController@home' );

Route::get('/register', function () {
    return view('register');
});
Route::post('/register', 'AuthController@register' );

Route::get('/adminlte/master', function () {
    return view('adminlte/master');
});

Route::get('/table', function () {
    return view('table');
});

Route::get('/data-tables', function () {
    return view('data-tables');
});

Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::post('/cast', 'CastController@index');
Route::get('/cast/{id}', 'CastController@show');
Route::get('/cast/{id}/edit', 'CastController@edit');
Route::put('/cast/{id}', 'CastController@update');
Route::delete('/cast/{id}', 'CastController@destroy');


