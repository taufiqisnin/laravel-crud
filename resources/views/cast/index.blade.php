@extends('adminlte/master')
@section('content')
<div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">List data para pemain film</h3>

    
        <div class="card-tools">
          <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
              <button type="submit" class="btn btn-default">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0" style="height: 300px;">
        <a class="btn btn-primary mb-2" href="cast/create">Tambah Cast Baru</a>
        <table class="table table-head-fixed text-nowrap">
          <thead>
            <tr>
              <th>ID</th>
              <th>Nama</th>
              <th>Umur</th>
              <th>Bio</th>
              <th>actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach($cast as $key => $cast)
            <tr>
              <td>{{$key + 1}} </td>
              <td>{{$cast->nama}}</td>
              <td>{{$cast->umur}}</td>
              <td>{{$cast->bio}}</td>
              <td style="display: flex;">
                <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">Show</a>
                <a href="/cast/{{$cast->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                <form action="/cast/{{$cast->id}}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
              </td>
            @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
  
@endsection
  @push('scripts')
  <script src="{{asset('/admin/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
  @endpush